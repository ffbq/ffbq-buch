"Buch" ist in dem Sinne ein allgemeine Formulierung für die Absicht Informationen zur
Fehrmarn Belt Querung zusammenzustellen:

* Jede:r kann etwas beitragen
* Die Inhalte werden unter einer freien Lizenz weitergegeben
* Es können neben der [Website](https://ffbq.gitlab.io/ffbq-buch/) auch Broschüren oder Bücher entstehen
