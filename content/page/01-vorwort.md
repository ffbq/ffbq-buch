---
title: "Vorwort"
---

Die Feste Fehmarnbeltquerung ist ein Traum von Verkehr- und Wirtschaftsplaner:innen seit Jahrzehnten. Aber nach wie vor nicht vollendet und bislang nicht verworfen.

Wir wollen in dieser Broschüre versuchen einen kleinen Überblick darüber zu geben, wie das Projekt aussieht und was es vor allem aus Klimagesichtspunkten bedeutet.

Wir haben nicht versucht alle Aspekte zu berücksichtigen. Denn es gibt Unzählige und die Geschichte ist lang. Und daher schreckt das Thema auch Viele. Nichtsdesdotrotz ist es als größtes Infrastrukturprojekt Europas ein Thema man nicht vorbei kommt, wenn man Klima- und Verkehrspolitik ernst nehmen will. 

Diese Broschüre versucht daher die wichtigsten Aspekte im Hinblick au die heutigen Debatten zusammenzufassen und zu erläutern als Argumentationshilfe für Gegner:innen des Fehmarnbelttunnels.

Denn dagegen sein muss man aus unserer Sicht


Diese Broschüre ist frei kopierbar unter eine Creative Commons-Lizenz und soll in Zukunft weiter gemeinsam erweitert werden.

Ziel ist es auch die Informationen aktuell zu halten, damit sie den jeweiligen Planungs- und Gutachtenstand nach wie vor repräsentieren. Es gibt viele Aktivist:innen, die teilweise seit Jahrzehnten mit dem Thema beschäftigt sind und jede Organisation pflegt ihre eigenen Texte. Dieses Projekt ist auch ein Angebot diese Informationen zu übernehmen und weiter zu entwickelna.

Die Texte werden in Gitlab als Hugo-Website in Markdown geschrieben und anschließend mit Pandoc zu eBooks und PDFs konvertiert.
