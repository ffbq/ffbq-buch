---
title: "Welche Fragen zur FFBQ"
---
# Welche Fragen?

## Welchen Aufbau hat der Tunnel?

Vier Tunnelröhren

* 2 x Schiene
* 4 x Autofahrspuren

## Verkehr 
-  Bedarfsanalyse und Prognosen 
 - Auto 
 - Eisenbahn 

 - CO² Prognosen....


## Wann sollte der Tunnel fertiggestellt werden? Wann wird er nach heutigem Stand (8.9.) fertiggestellt? 

* Zunächst 2021/22 nach sechjähriger Bauzeit

(Quelle: https://de.wikipedia.org/wiki/Fehmarnbelttunnel#cite_note-29)


> Der Bau des Fehmarnbelt-Tunnels wird ca. 8,5 Jahre dauern.

(Quelle: https://femern.com/de/Tunnel/Facts-on-the-tunnel)

## Wie teuer wird der Tunnel ?

* Zunächst 5 Mrd geschätzt
* Seit 2015 9 Mrd

### Wie teuer für Deutschland

> Für die Anbindung des Tunnels zum Hinterland müssen bei Realisierung des Projekts Autobahnen, Brücken und Eisenbahnstrecken bis nach Hamburg neu gebaut bzw. ausgebaut werden. Nach Zahlen aus dem Jahr 2015 seien dafür auf deutscher Seite mindestens 3 Milliarden Euro vom deutschen Steuerzahler bzw. der EU aufzubringen.
(quelle: https://de.wikipedia.org/wiki/Fehmarnbelttunnel#cite_note-33)


## Wer trägt die Baukosten?

## Welche Nachteile hatte die Einstellung der Verbindung via Fehmarn?

* Die Halte in Schleswig-Holstein fallen komplett weg, wie zB Lübeck
* Der Bahnhof "Timmendorfer Strand" und die Strecke sollen abgekoppelt werden. Tourist:innen und Berufspendler sollen dann mit dem Bus zu einem neuen Bahnhof fahren und dort umsteigen.

## Seit wann und wie lange fahren die Eurocitys nicht mehr über Rodby, sondern über Padborg?

5.12.2019

## Wie lange dauerte eine Fahrt mit dem Eurocity vor dem Baubeginn?

Hamburg - Kopenhagen ab > 5 Stunden

> "Für Fernreisende wird die Fahrt damit sogar schneller. Sie sparen rund eine Viertelstunde Reisezeit"
(Quelle 3)


> Die feste Verbindung würde die Fahrzeit von Hamburg nach Kopenhagen (340 km) von 4:30 Stunden (Reisezeit 2008 inklusive Fähre, Wartezeit, Ein- und Ausschiffung) auf 2:40 Stunden und die von Personenzügen sogar auf 2:30 Stunden reduzieren.
(Quelle 4)

### Quelle:
* 1 [Keine Mitfahrgelegenheit: Eurocity-Züge rasen ohne Halt durch SH](https://www.shz.de/regionales/schleswig-holstein/keine-mitfahrgelegenheit-eurocity-zuege-rasen-ohne-halt-durch-sh-id22989107.html) SHZ  14.3.2019
* 2 [Für Bahnreisende drohen große Einschränkungen](https://www.ln-online.de/Lokales/Ostholstein/Fuer-Bahnreisende-drohen-grosse-Einschraenkungen) Lübecker Nachrichten vom 9.5.2019
* 3 [Bahn stoppt Verkehr über den Fehmarnbelt](https://www.ln-online.de/Nachrichten/Norddeutschland/Deutsche-Bahn-stoppt-Zuege-die-per-Scandlines-Faehre-ueber-den-Fehmarnbelt-fahren) LN vom 5.3.2019

* 4 Wikipedia: https://de.wikipedia.org/wiki/Feste_Fehmarnbeltquerung#Geographische_Situation_und_Geschichte


## Vorteile der Fährverbindung

* Kann für LKW-Fahrer:innen für die vorgeschriebenen Pausenzeiten genutzt werden, sie können aber dennoch vorankommen
* Auch bei geringem Bedarf kann dies weiterbetrieben werden. Oder Fähren anderweiteig eingesetzt. Man verbnaut keine Infrastruktur, sondern nutzt vorhandene


## ZU Bedenken

* Die Verkürzung der Reisezeit gilt primär für die Verbindung Hamburg-Kopenhagen - je länger die Distanz desto mehr kommen andere Faktoren zum tragen und desto geringer sind die Zeiteinsparungen. zB kommt dann vermehrt zum tragen, das der Bahnverkehr in Deutschland nach wie vor viele Defizite aufweist. Noch krasser ist es beim Güterverkehr. Weil dort oft lange Standzeiten vorkommen. Diese Strecke ist kein primärer Flaschenhals. Und zudem findet der überwiegende Güterverkehr auf der Schiene innerhalb Deutschlands statt und nur wenig nach Skandinavien.
* Die Gütermengen nehmen eher ab als zu. Zudem ist zu erwarten, dass ein verschärfter Kurs auf die Einhaltung der Klimaziele einen noch stärkeren Einbruch bedeutet. 


## Verkehrssteigerung laut IHK
> Von den zum Zeitpunkt der Eröffnung erwarteten 7.904 PKW  täglich,  passieren  heute  bereits  4.216  Autos  die Insel.  Bei  Bussen  und  LKWs  fällt  die  Steigerung  noch geringer aus. Während heute 1.067 LKW und 79 Busse täglich fahren, werden für das Jahr nach der Eröffnung 1.521 LKW und 93 Busse prognostiziert.42


