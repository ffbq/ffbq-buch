---
title: FFBQ Organisationen
---

# Welche Organisationen gibt es?

(alphabetisch)

## AG Belt Hamburg

Anschrift: 
Bodo Gehrke  
Neustädter Str. 57a  
23730 Roge  

* Web: https://stop-fehmarnbelttunnel.de/

## Aktionsbündnis gegen feste Fehmarnbeltquerung e.V.

* Gründungsjahr 1994

>Seit 1994 hat das „Aktionsbündnis gegen eine feste
>Fehmarnbeltquerung“ die Vorplanungen für die feste
>Fehmarnbeltquerung als Bürgerinitiative mit kritischen
>Stellungnahmen zu deren nachteiligen Auswirkungen auf
>die Umwelt und Bevölkerung sowie mit Aktionen zur
>Sensibilisierung der Bevölkerung begleitet.

Anschrift:
Hendrick Kerlen, Vorsitzender
Westermarkelsdorf 12 A
23769 Fehmarn

* Web: https://beltquerung.info/

## Allianz gegen eine Feste Fehmarnbeltquerung

* Gründungsjahr: 2010 ?

>Zusammenschluss von
>10 Bürgerinitiativen und
>Vereinen aus Ostholstein
>und Fehmarn

Anschrift:

Allianz gegen eine feste Fehmarnbeltquerung e.V. 
Hof Altona  
23730 Sierksdorf  

* Web: http://allianz-beltquerung.info/

Mitglieder:
* AG Belt Hamburg


## Beltretter e.V.

> Wir retten jetzt eine ganze Region vor der größten Baustelle Nord-Europas. Und vor ihren dramatischen Folgen für so viele Menschen, ja sogar für ganz Deutschland. Denn die Euro-Milliarden würden für andere, für viel wichtigere Projekte fehlen. 

Anschrift:

* Web: https://beltretter.de/


## Fehmarn-Beltverkehr
* Gründungsjahr

Anschrift:


* Facebook: https://www.facebook.com/BI-Fehmarn-Beltverkehr-2331916190424747/


## Kein Güterbahnverkehr durch die Badeorte der Lübecker Bucht


## Landesnaturschutzverband Schleswig-Holstein

Anschrift:
Landesnaturschutzverband Schleswig-Holstein e.V.  
Burgstraße 4  
24103 Kiel  

* Web: https://lnv-sh.de/

## PRO BAHN Landesverband Schleswig-Holstein/ Hamburg e.V.

Anschrift:
PRO BAHN Landesverband Schleswig-Holstein/ Hamburg e.V.
Lerchenstraße 18-20 (c/o VCD e.V., Landesverband Nord)
24103 Kiel

* Web: http://pro-bahn-sh.de/


